Resources:
 - https://stackoverflow.com/questions/6787824/mysql-command-line-formatting-with-utf8
 - https://stackoverflow.com/questions/59714736/how-to-change-column-charset-and-collation-with-doctrine
 - https://github.com/doctrine/orm/issues/7485#issuecomment-441248544
 - https://avris.it/blog/emojis-in-doctrine

# First install

`docker-compose exec php bash`

then 

`docker-php-ext-install pdo pdo_mysql`

then

`./bin/console doctrine:database:create && ./bin/console do:mi:mi`

# Test

1. Create data using utf8_general_ci

`./bin/console doctrine:fixtures:load --append`

2. Modify config/packages/doctrine.yaml

```
        charset: utf8mb4
        default_table_options:
            charset: utf8mb4
            collate: utf8mb4_unicode_ci
```

3. Re run fixtures

`./bin/console doctrine:fixtures:load --append`
