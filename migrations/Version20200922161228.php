<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200922161228 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE latin1_gen_ci (id INT AUTO_INCREMENT NOT NULL, field_utf8_gen_ci VARCHAR(255) CHARACTER SET utf8 DEFAULT NULL COLLATE `utf8_general_ci`, field_utf8_uni_ci VARCHAR(255) CHARACTER SET utf8 DEFAULT NULL COLLATE `utf8_unicode_ci`, field_utf8mb4_uni_ci VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, field_latin1_swed_ci VARCHAR(255) CHARACTER SET latin1 DEFAULT NULL COLLATE `latin1_swedish_ci`, field_latin1_gen_ci VARCHAR(255) CHARACTER SET latin1 DEFAULT NULL COLLATE `latin1_general_ci`, PRIMARY KEY(id)) DEFAULT CHARACTER SET latin1 COLLATE `latin1_general_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE latin1_swed_ci (id INT AUTO_INCREMENT NOT NULL, field_utf8_gen_ci VARCHAR(255) CHARACTER SET utf8 DEFAULT NULL COLLATE `utf8_general_ci`, field_utf8_uni_ci VARCHAR(255) CHARACTER SET utf8 DEFAULT NULL COLLATE `utf8_unicode_ci`, field_utf8mb4_uni_ci VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, field_latin1_swed_ci VARCHAR(255) CHARACTER SET latin1 DEFAULT NULL COLLATE `latin1_swedish_ci`, field_latin1_gen_ci VARCHAR(255) CHARACTER SET latin1 DEFAULT NULL COLLATE `latin1_general_ci`, PRIMARY KEY(id)) DEFAULT CHARACTER SET latin1 COLLATE `latin1_swedish_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE utf8_gen_ci (id INT AUTO_INCREMENT NOT NULL, field_utf8_gen_ci VARCHAR(255) CHARACTER SET utf8 DEFAULT NULL COLLATE `utf8_general_ci`, field_utf8_uni_ci VARCHAR(255) CHARACTER SET utf8 DEFAULT NULL COLLATE `utf8_unicode_ci`, field_utf8mb4_uni_ci VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, field_latin1_swed_ci VARCHAR(255) CHARACTER SET latin1 DEFAULT NULL COLLATE `latin1_swedish_ci`, field_latin1_gen_ci VARCHAR(255) CHARACTER SET latin1 DEFAULT NULL COLLATE `latin1_general_ci`, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_general_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE utf8_uni_ci (id INT AUTO_INCREMENT NOT NULL, field_utf8_gen_ci VARCHAR(255) CHARACTER SET utf8 DEFAULT NULL COLLATE `utf8_general_ci`, field_utf8_uni_ci VARCHAR(255) CHARACTER SET utf8 DEFAULT NULL COLLATE `utf8_unicode_ci`, field_utf8mb4_uni_ci VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, field_latin1_swed_ci VARCHAR(255) CHARACTER SET latin1 DEFAULT NULL COLLATE `latin1_swedish_ci`, field_latin1_gen_ci VARCHAR(255) CHARACTER SET latin1 DEFAULT NULL COLLATE `latin1_general_ci`, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE utf8mb4_uni_ci (id INT AUTO_INCREMENT NOT NULL, field_utf8_gen_ci VARCHAR(255) CHARACTER SET utf8 DEFAULT NULL COLLATE `utf8_general_ci`, field_utf8_uni_ci VARCHAR(255) CHARACTER SET utf8 DEFAULT NULL COLLATE `utf8_unicode_ci`, field_utf8mb4_uni_ci VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, field_latin1_swed_ci VARCHAR(255) CHARACTER SET latin1 DEFAULT NULL COLLATE `latin1_swedish_ci`, field_latin1_gen_ci VARCHAR(255) CHARACTER SET latin1 DEFAULT NULL COLLATE `latin1_general_ci`, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE latin1_gen_ci');
        $this->addSql('DROP TABLE latin1_swed_ci');
        $this->addSql('DROP TABLE utf8_gen_ci');
        $this->addSql('DROP TABLE utf8_uni_ci');
        $this->addSql('DROP TABLE utf8mb4_uni_ci');
    }
}
