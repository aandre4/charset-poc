<?php

namespace App\DataFixtures;

use App\Entity\EntityInterface;
use App\Entity\Latin1GenCi;
use App\Entity\Latin1SwedCi;
use App\Entity\Utf8GenCi;
use App\Entity\Utf8Mb4UniCi;
use App\Entity\Utf8UniCi;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    private function getEntities(): array
    {
        return [
            new Latin1GenCi(),
            new Latin1SwedCi(),
            new Utf8GenCi(),
            new Utf8Mb4UniCi(),
            new Utf8UniCi(),
        ];
    }

    public function load(ObjectManager $manager)
    {
        $entities = $this->getEntities();
//        $data = '😎';//Kid Interiør Skadeförsäkring 象形字 xiàngxíngzì';
        $data = 'Kid Interiør Skadeförsäkring xiàngxíngzì';
        /** @var EntityInterface $entity */
        foreach ($entities as $entity) {
            $entity->setFieldUtf8GenCi($data);
            $entity->setFieldUtf8UniCi($data);
            $entity->setFieldUtf8Mb4UniCi($data);
            $entity->setFieldLatin1SwedCi($data);
            $entity->setFieldLatin1GenCi($data);
            $manager->persist($entity);
        }

        $manager->flush();

        foreach ($entities as $entity) {
            $className = get_class($entity);
            $entries = $manager->getRepository($className)->findAll();
            dump($entries);
        }
    }
}
