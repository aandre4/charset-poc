<?php
namespace App\Entity;

interface EntityInterface
{
    public function getId(): int;

    public function setId(int $id): void;

    public function getFieldUtf8GenCi(): string;

    public function setFieldUtf8GenCi(string $fieldUtf8GenCi): void;

    public function getFieldUtf8UniCi(): string;

    public function setFieldUtf8UniCi(string $fieldUtf8UniCi): void;

    public function getFieldUtf8Mb4UniCi(): string;

    public function setFieldUtf8Mb4UniCi(string $fieldUtf8Mb4UniCi): void;

    public function getFieldLatin1SwedCi(): string;

    public function setFieldLatin1SwedCi(string $fieldLatin1SwedCi): void;

    public function getFieldLatin1GenCi(): string;

    public function setFieldLatin1GenCi(string $fieldLatin1GenCi): void;
}
