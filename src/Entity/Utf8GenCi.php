<?php
namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="utf8_gen_ci", options={"charset"="utf8", "collate"="utf8_general_ci"})
 */
class Utf8GenCi implements EntityInterface
{
    /**
     * @var int
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="field_utf8_gen_ci", type="string", options={"charset"="utf8", "collation"="utf8_general_ci"}, nullable=true)
     */
    private $fieldUtf8GenCi;

    /**
     * @var string
     * @ORM\Column(name="field_utf8_uni_ci", type="string", options={"charset"="utf8", "collation"="utf8_unicode_ci"}, nullable=true)
     */
    private $fieldUtf8UniCi;

    /**
     * @var string
     * @ORM\Column(name="field_utf8mb4_uni_ci", type="string", options={"charset"="utf8mb4", "collation"="utf8mb4_unicode_ci"}, nullable=true)
     */
    private $fieldUtf8Mb4UniCi;

    /**
     * @var string
     * @ORM\Column(name="field_latin1_swed_ci", type="string", options={"charset"="latin1", "collation"="latin1_swedish_ci"}, nullable=true)
     */
    private $fieldLatin1SwedCi;

    /**
     * @var string
     * @ORM\Column(name="field_latin1_gen_ci", type="string", options={"charset"="latin1", "collation"="latin1_general_ci"}, nullable=true)
     */
    private $fieldLatin1GenCi;

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function getFieldUtf8GenCi(): string
    {
        return $this->fieldUtf8GenCi;
    }

    public function setFieldUtf8GenCi(string $fieldUtf8GenCi): void
    {
        $this->fieldUtf8GenCi = $fieldUtf8GenCi;
    }

    public function getFieldUtf8UniCi(): string
    {
        return $this->fieldUtf8UniCi;
    }

    public function setFieldUtf8UniCi(string $fieldUtf8UniCi): void
    {
        $this->fieldUtf8UniCi = $fieldUtf8UniCi;
    }

    public function getFieldUtf8Mb4UniCi(): string
    {
        return $this->fieldUtf8Mb4UniCi;
    }

    public function setFieldUtf8Mb4UniCi(string $fieldUtf8Mb4UniCi): void
    {
        $this->fieldUtf8Mb4UniCi = $fieldUtf8Mb4UniCi;
    }

    public function getFieldLatin1SwedCi(): string
    {
        return $this->fieldLatin1SwedCi;
    }

    public function setFieldLatin1SwedCi(string $fieldLatin1SwedCi): void
    {
        $this->fieldLatin1SwedCi = $fieldLatin1SwedCi;
    }

    public function getFieldLatin1GenCi(): string
    {
        return $this->fieldLatin1GenCi;
    }

    public function setFieldLatin1GenCi(string $fieldLatin1GenCi): void
    {
        $this->fieldLatin1GenCi = $fieldLatin1GenCi;
    }
}
